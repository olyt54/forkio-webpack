const path = require("path"),
    HTMLWebpackPlugin = require('html-webpack-plugin'),
    {CleanWebpackPlugin} = require('clean-webpack-plugin'),
    miniCssExtractPlugin = require("mini-css-extract-plugin"),
    copyWebpackPlugin = require('copy-webpack-plugin'),
    ImageminPlugin = require('imagemin-webpack-plugin').default;

const config = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "scripts.min.js",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ],
                    }
                }]

            },
            {
                test: /\.scss$/,
                use: [
                    miniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: "postcss-loader",
                        options: {config: {path: "./postcss.config.js"}}
                    },
                    {
                        loader: 'sass-loader'
                    },

                ]
            },
            {
                test: /\.(png|jpg|jpeg|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                },
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HTMLWebpackPlugin({
            template: './src/index.html'
        }),
        new miniCssExtractPlugin({
            filename: "styles.min.css",
        }),
        new copyWebpackPlugin({
            patterns: [
                { from: path.resolve(__dirname, "./src/img"), to: path.resolve(__dirname, "./dist/img") }
            ]
        }),
        new ImageminPlugin({ test: /\.(png|jpg|jpeg|svg)$/i })
    ],
};

module.exports = config;